import React from 'react';
import { useMediaQuery, ChakraProvider, Box, Grid, Button, Flex, Spacer, Heading, Text, Stack, Image } from '@chakra-ui/react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import theme from "./theme";
import logo from "./assets/LOGO.svg";
import pic1 from "./assets/undraw_learning_2q1h.svg";
import pic2 from "./assets/undraw_book_reading_kx9s.svg";
import pic3 from "./assets/undraw_true_friends_c94g.svg";
import qualityEducation from './assets/quality_education.svg'
import highAchievingStudents from './assets/high_achieving_students.svg'
import digitalNotes from './assets/digital_notes.svg'
import onlineLearning from './assets/online_learning.svg'
import workshops from './assets/workshops.svg'
import interactiveLessons from './assets/interactive_lessons.svg'
import tipsTricks from './assets/tips_and_tricks.svg'

function App() {
	const [isLargerThan1280] = useMediaQuery("(min-width: 1280px)");

	return (
		<ChakraProvider theme={theme}>
			<Header mediaQuery={isLargerThan1280} />
			{
				isLargerThan1280 ? <></> : <Header mediaQuery={isLargerThan1280} placeholderStatus={true} />
			}
			<Box h={isLargerThan1280 ? "100vh" : "auto"} w="100%" minH={isLargerThan1280 ? "initial" : "100"}>
				<Flex flexDirection="column" h="100%">
					<Spacer />
					<Box py={10} px={isLargerThan1280 ? 44 : 12}>
						<Grid templateColumns={isLargerThan1280 ? "50% 50%" : "100% 0"}>
							<Box>
								<Heading pb={10} fontSize="5xl">Prepare yourself for a future of unlimited opportunities.</Heading>
								<Text fontSize="lg" pb={12}>
									Do you come from an underprivileged background but hope that you can succeed academically?<br />
									Do you feel bad going through hours of tuition and extra classes, yet still struggle to understand the syllabus?<br />
									If that’s what you are facing, you’ve found the treasure.
                				</Text>
								<Stack direction="row" align="center" spacing={4}>
									<Button size="lg" px={10} colorScheme="brand" color="gray.800" aria-label="Explore">Sign Up</Button>
									<Button size="lg" px={8} variant="outline" colorScheme="black" aria-label="Learn More" onClick={() => { window.open("https://forms.gle/r4aznhKEA5qCuuRk6", "_blank") }}>Contact Us</Button>
								</Stack>
							</Box>
							<Box>
								<Flex flexDirection="column" h="100%">
									<Spacer />
									<Flex justify="flex-end">
										<Image src={pic1} boxSize={400} alt="Picture of learning" draggable={false} />
									</Flex>
									<Spacer />
								</Flex>
							</Box>
						</Grid>
					</Box>
					<Spacer />
				</Flex>
			</Box>
			<Box pt={isLargerThan1280 ? 10 : 24} pb={10}>
				<Flex>
					<Spacer />
					<Box w="60%">
						<Heading fontSize="4xl" align="center" pb={10}>
							Why Project Nactus exists
            			</Heading>
						<Text fontSize="lg" align="center" pb={12}>
							We are a team of highly passionate high-school graduates looking to eradicating
							socioeconomic barriers and levelling the playing field for students who are determined
							to succeed in their academics. A quality education is an important criteria in today’s
							generation, for example, universities require students to achieve a certain degree of
							academic maturity to gain admission. According to statistics, 45.9% of youth in
							Malaysia live below the poverty line (Khazanah Research Institute, 2019). Many people
							may lose opportunities due to this socioeconomic barrier, thus they are unable to
							expand their potential despite having the capability. Thus, this non-profit project
							aims to provide a virtual platform that can help level the playing field for students,
							so that they can leap over this small yet significant gap, build self-confidence and
							expand their boundaries.
            			</Text>
					</Box>
					<Spacer />
				</Flex>
			</Box>
			<Box px={isLargerThan1280 ? 44 : 12} pt={10} pb={10}>
				<Grid templateColumns={isLargerThan1280 ? "50% 50%" : ""} templateRows={isLargerThan1280 ? "" : "50% 50%"}>
					<Box display={isLargerThan1280 ? "none" : ""}>
						<Flex justify="flex-end">
							<Image src={pic3} boxSize={400} alt="Picture of friends" draggable={false} />
						</Flex>
					</Box>
					<Box>
						<Flex flexDirection="column" h="100%">
							<Spacer />
							<Box>
								<Heading pb={10} fontSize="4xl">Our Mission</Heading>
								<Text fontSize="lg" pb={12}>
									To provide free quality education to underprivileged students.<br />
									To help underperforming students to realize their potential to
									succeed in their academics.
                				</Text>
							</Box>
							<Spacer />
						</Flex>
					</Box>
					<Box display={isLargerThan1280 ? "" : "none"}>
						<Flex justify="flex-end">
							<Image src={pic3} boxSize={400} alt="Picture of friends" draggable={false} />
						</Flex>
					</Box>
				</Grid>
			</Box>
			<Box px={isLargerThan1280 ? 44 : 12} pt={10} pb={10}>
				<Grid templateColumns={isLargerThan1280 ? "50% 50%" : ""} templateRows={isLargerThan1280 ? "" : "50% 50%"}>
					<Box>
						<Flex>
							<Image src={pic3} boxSize={400} alt="Picture of friends" draggable={false} />
						</Flex>
					</Box>
					<Box>
						<Flex flexDirection="column" h="100%">
							<Spacer />
							<Box>
								<Heading pb={10} fontSize="4xl">Our Vision</Heading>
								<Text fontSize="lg" pb={12}>
									To provide an equal playing field in terms of academics regardless
									of one’s socioeconomic background
                				</Text>
							</Box>
							<Spacer />
						</Flex>
					</Box>
				</Grid>
			</Box>
			<Box px={isLargerThan1280 ? 44 : 12} pt={10} pb={10}>
				<Grid templateColumns={isLargerThan1280 ? "25% 25% 25% 25%" : "100%"} py={isLargerThan1280 ? 10 : 0}>
					<Box px={4} py={4}>
						<Box>
							<Flex justifyContent="center">
								<Image src={qualityEducation} boxSize={150} alt="Picture of reading a book" draggable={false} />
							</Flex>
						</Box>
						<Box textAlign="center" mt={4}>
							Get quality education free of charge
						</Box>
					</Box>
					<Box px={4} py={4}>
						<Flex justifyContent="center">
							<Image src={highAchievingStudents} boxSize={150} alt="Picture of reading a book" draggable={false} />
						</Flex>
						<Box textAlign="center" mt={4}>
							Learn from recently graduated high achieving students
						</Box>
					</Box>
					<Box px={4} py={4}>
						<Flex justifyContent="center">
							<Image src={digitalNotes} boxSize={150} alt="Picture of reading a book" draggable={false} />
						</Flex>
						<Box textAlign="center" mt={4}>
							Free digital notes are provided
						</Box>
					</Box>
					<Box px={4} py={4}>
						<Flex justifyContent="center">
							<Image src={onlineLearning} boxSize={150} alt="Picture of reading a book" draggable={false} />
						</Flex>
						<Box textAlign="center" mt={4}>
							Learn online
						</Box>
					</Box>
				</Grid>
				<Grid templateColumns={isLargerThan1280 ? "12.5% 25% 25% 25% 12.5%" : "100%"} py={isLargerThan1280 ? 10 : 0}>
					<Box></Box>
					<Box px={4} py={4}>
						<Flex justifyContent="center">
							<Image src={workshops} boxSize={150} alt="Picture of reading a book" draggable={false} />
						</Flex>
						<Box textAlign="center" mt={4}>
							Occasional workshops with experienced individuals in their respective fields
						</Box>
					</Box>
					<Box px={4} py={4}>
						<Flex justifyContent="center">
							<Image src={interactiveLessons} boxSize={150} alt="Picture of reading a book" draggable={false} />
						</Flex>
						<Box textAlign="center" mt={4}>
							Interactive and engaging lessons to retain attention throughout the class
						</Box>
					</Box>
					<Box px={4} py={4}>
						<Flex justifyContent="center">
							<Image src={tipsTricks} boxSize={150} alt="Picture of reading a book" draggable={false} />
						</Flex>
						<Box textAlign="center" mt={4}>
							Tried and true tips and tricks to retain memory of the concepts taught
						</Box>
					</Box>
					<Box></Box>
				</Grid>
			</Box>
			<Box pt={10} pb={32} px={isLargerThan1280 ? 44 : 12}>
				<Grid templateColumns={isLargerThan1280 ? "50% 50%" : "0 100%"}>
					<Box>
						<Flex flexDirection="column" h="100%">
							<Spacer />
							<Image src={pic2} boxSize={400} alt="Picture of reading a book" draggable={false} />
							<Spacer />
						</Flex>
					</Box>
					<Box>
						<Flex flexDirection="column" h="100%">
							<Spacer />
							<Box>
								<Flex direction={isLargerThan1280 ? "row" : "column"}>
									<Box flex="1" py={6}>
										<Image src={logo} boxSize={20} mb={8} alt="Nactus Logo" draggable={false} />
										<Heading fontSize="lg" pb={4}>
											Phase 1
                  					</Heading>
										<Text fontSize="md">
											Sign up for phase 1 of our lessons completely free of charge to gain
											exclusive access to our course material. (For students of Chung Ling
											High School only)
                  					</Text>
									</Box>
									<Box flex="0.15">
										&nbsp;
									</Box>
									<Box flex="1" py={6}>
										<Image src={logo} boxSize={20} mb={8} alt="Nactus Logo" draggable={false} />
										<Heading fontSize="lg" pb={4}>
											Phase 2
                  					</Heading>
										<Text fontSize="md">
											Registration starts 31st December 2021. Open to all students of ages
											16 and 17.
                  					</Text>
									</Box>
								</Flex>
							</Box>
							<Spacer />
						</Flex>
					</Box>
				</Grid>
			</Box>
			<Footer mediaQuery={isLargerThan1280} />
		</ChakraProvider>
	);
}

export default App;
