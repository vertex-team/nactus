import React from 'react';
import { Box, Grid, Image, Stack, IconButton, Flex, Spacer } from '@chakra-ui/react';
import textLogo from "../assets/TEXTLOGO.svg";
import { IoLogoFacebook, IoLogoInstagram, IoMail } from "react-icons/io5";

function Footer(props) {
    return (
        <Box>
            <Box w="100%" py={14} px={props.mediaQuery ? 28 : 0} bg="white">
                <Grid templateColumns={props.mediaQuery ? "20% 80%" : "100%"}>
                    <Box display={props.mediaQuery ? "initial" : "none"} h={16}>
                        <Image src={textLogo} alt="Nactus Logo" draggable={false} boxSize="100%"/>
                    </Box>
                    <Box textAlign="right">
                        <Flex justify="flex-end" flexDirection="column" h="100%">
                            <Spacer />
                            <Stack direction="row" align="center" justify={props.mediaQuery ? "flex-end" : "center"} spacing={6} textAlign="right">
                                <IconButton icon={<IoLogoFacebook />} variant="ghost" fontSize="3xl" aria-label="Facebook Page" />
                                <IconButton icon={<IoLogoInstagram />} variant="ghost" fontSize="3xl" aria-label="Instagram Page" />
                                <IconButton icon={<IoMail />} variant="ghost" fontSize="3xl" aria-label="Email Us" />
                            </Stack>
                            <Spacer />
                        </Flex>
                    </Box>
                </Grid>
            </Box>
        </Box>
    );
}

export default Footer;