import React from 'react';
import { Box, Grid, Image, Button, Center } from '@chakra-ui/react';
import logo from "../assets/LOGO.svg";

function Header(props) {
    return (
        props.placeholderStatus ?
            <Box w="100%" py={10} bg="white" opacity={0}>
                <Grid templateColumns={props.mediaQuery ? "65% 35%" : "100%"}>
                    <Box>
                        {
                            props.mediaQuery ? <Image src={logo} boxSize="54" ml={24} alt="Nactus Logo" draggable={false}/> : <Center><Image src={logo} boxSize="54" alt="Nactus Logo" draggable={false}/></Center>
                        }
                    </Box>
                    <Box textAlign="right" pr={20} display={props.mediaQuery ? "initial" : "none"}>
                        <Button px={10} py={6} colorScheme="brand" color="gray.800">Sign Up</Button>
                    </Box>
                </Grid>
            </Box>
            :
            <Box w="100%" py={10} position="fixed" bg="white" zIndex={10}>
                <Grid templateColumns={props.mediaQuery ? "65% 35%" : "100%"}>
                    <Box>
                        {
                            props.mediaQuery ? <Image src={logo} boxSize="54" ml={24} alt="Nactus Logo" draggable={false}/> : <Center><Image src={logo} boxSize="54" alt="Nactus Logo" draggable={false}/></Center>
                        }
                    </Box>
                    <Box textAlign="right" pr={20} display={props.mediaQuery ? "initial" : "none"}>
                        <Button px={10} py={6} colorScheme="brand" color="gray.800" aria-label="Sign Up">Sign Up</Button>
                    </Box>
                </Grid>
            </Box>
    );
}

export default Header;